Copiar y pegar para cambiar entre configuraciones los siguientes archivos:

- AndroidManifest.xml
- build.gradle
- google-services.json

Copiamos el de la version free o premium a su correspondiente fichero de la carpeta superior

También hay que modificar los archivos:
- main.cpp
- Info.qml

Se desconecta la parte de free o premium dependiendo de la release que se quiera generar