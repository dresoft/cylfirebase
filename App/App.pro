TEMPLATE = app

QT += gui qml quick multimedia webview
!no_desktop: QT += widgets

CONFIG += c++11

# Additional import path used to resolve QML modules in Qt Creator's code model
# QML_IMPORT_PATH =

# IMPORTANT must be included before extensions
# Default rules for deployment.
include(deployment.pri)

SOURCES += \
    main.cpp \
    algoritmos.cpp \
    diccionario.cpp \
    hilocargadiccionario.cpp \
    receiver.cpp


PLATFORMS_DIR = $$PWD/platforms

android: {

    ANDROID_PACKAGE_SOURCE_DIR = $$PLATFORMS_DIR/android

    DISTFILES += \
        $$ANDROID_PACKAGE_SOURCE_DIR/AndroidManifest.xml \
        $$ANDROID_PACKAGE_SOURCE_DIR/build.gradle \
        $$ANDROID_PACKAGE_SOURCE_DIR/gradle.properties \
        $$ANDROID_PACKAGE_SOURCE_DIR/local.properties \
        $$ANDROID_PACKAGE_SOURCE_DIR/google-services.json \
        $$ANDROID_PACKAGE_SOURCE_DIR/src/com/blackgrain/android/firebasetest/Main.java \
        $$ANDROID_PACKAGE_SOURCE_DIR/res/values/strings.xml

}

ios: {

    ios_icon.files = $$files($$PLATFORMS_DIR/ios/icons/AppIcon*.png)
    QMAKE_BUNDLE_DATA += ios_icon

    itunes_icon.files = $$files($$PLATFORMS_DIR/ios/iTunesArtwork*)
    QMAKE_BUNDLE_DATA += itunes_icon

    app_launch_images.files = $$PLATFORMS_DIR/ios/LauncherScreen.xib $$files($$PLATFORMS_DIR/ios/LaunchImage*.png) $$files($$PLATFORMS_DIR/ios/splash_*.png)
    QMAKE_BUNDLE_DATA += app_launch_images

    QMAKE_INFO_PLIST = $$PLATFORMS_DIR/ios/Info.plist

    DISTFILES += \
        $$PLATFORMS_DIR/ios/Info.plist \
        $$PLATFORMS_DIR/ios/GoogleService-Info.plist

    # You must deploy your Google Play config file
    deployment.files = $$PLATFORMS_DIR/ios/GoogleService-Info.plist
    deployment.path =
    QMAKE_BUNDLE_DATA += deployment

#    Q_ENABLE_BITCODE.name = ENABLE_BITCODE
#    Q_ENABLE_BITCODE.value = NO
#    QMAKE_MAC_XCODE_SETTINGS += Q_ENABLE_BITCODE
}

# Make these modules of QtFirebase
QTFIREBASE_SDK_PATH = /Users/casa/Projects/cylfirebase/extensions/QtFirebase/firebase_cpp_sdk
QTFIREBASE_CONFIG +=  analytics messaging admob remote_config
# include QtFirebase
include(../extensions/QtFirebase/qtfirebase.pri) # <- /path/to/QtFirebase/qtfirebase.pri

RESOURCES += \
    qml.qrc

HEADERS += \
    algoritmos.h \
    diccionario.h \
    hilocargadiccionario.h \
    receiver.h

DISTFILES += \
    platforms/android/res/drawable/splash.xml \
    platforms/android/res/values/apptheme.xml \
    platforms/android/res/values/libs.xml \
    platforms/android/res/values/strings.xml \
    platforms/android/res/drawable/splash.xml \
    platforms/android/res/values/apptheme.xml \
    platforms/android/res/values/libs.xml \
    platforms/android/res/values/strings.xml \
    platforms/android/res/drawable/icon.png \
    platforms/android/res/drawable/splash_image.png \
    platforms/android/res/drawable-hdpi/icon.png \
    platforms/android/res/drawable-hdpi/splash_image.png \
    platforms/android/res/drawable-ldpi/icon.png \
    platforms/android/res/drawable-ldpi/splash_image.png \
    platforms/android/res/drawable-mdpi/icon.png \
    platforms/android/res/drawable-mdpi/splash_image.png \
    platforms/android/google-services.json_Free.json \
    platforms/android/google-services.json_Premium.json \
    platforms/android/AndroidManifest.xml_Premium.xml \
    platforms/android/Configuraciones Premium y free/google-services.json_Free.json \
    platforms/android/Configuraciones Premium y free/google-services.json_Premium.json \
    platforms/android/Configuraciones Premium y free/AndroidManifest.xml_Free.xml \
    platforms/android/Configuraciones Premium y free/AndroidManifest.xml_Premium.xml \
    platforms/android/Configuraciones Premium y free/build.gradle_Free \
    platforms/android/Configuraciones Premium y free/build.gradle_Premium \
    platforms/android/Configuraciones Premium y free/Readme.txt
