.pragma library

var rondaActual = 1
var maxRondas = 10
var ordenPartida = ['C','L','L','C','L','L','C','L','L','C']
var puntuacionesRondas = []
var puntuacionCifras = 0
var puntuacionLetras = 0
var puntuacionTotal = 0

function resetPartida()
{
    rondaActual = 1
    puntuacionesRondas = []
    puntuacionCifras = 0
    puntuacionLetras = 0
    puntuacionTotal = 0
}

function finalizarRonda(puntuacion) {

    puntuacionesRondas.push(puntuacion)

    if (ordenPartida[rondaActual-1] === 'C')
        puntuacionCifras += puntuacion
    else
        puntuacionLetras += puntuacion
    puntuacionTotal = puntuacionCifras + puntuacionLetras
    rondaActual++
}

function queTocaAhora()
{
    return ordenPartida[rondaActual-1]
}

