//LETRAS INICIALES Y OPERACIONES CON LAS LETRAS
console.log("INICIO creación Letras.js")

//Consontantes iniciales
var consonantesIniciales = ['B','B','B','B','B','C','C','C','C','D','D','D','D','D']
consonantesIniciales.push('F','F','F','G','G','G','H','H','J','J','K','L','L','L','L')
consonantesIniciales.push('M','M','M','N','N','N','Ñ','P','P','P','P','Q','R','R','R')
consonantesIniciales.push('R','S','S','S','S','S','T','T','T','V','V','W','X','Y','Z','Z')

//Vocales Iniciales
var vocalesIniciales = ['A','A','A','E','E','E','I','I','O','O','U','U']

var consonantesRestantes = []
var vocalesRestantes = []

var movimientos = []
var letrasPartida = []

var numVocales = 0
var numConsonantes = 0

generaPartida()
console.log("FIN creación Letras.js")

//Obtener letras
function getConsonante ()
{
    var indice = Math.floor(Math.random() * (consonantesRestantes.length - 1))
    var cons = consonantesRestantes[indice]
    consonantesRestantes.splice(indice,1)
    return cons
}

function getVocal()
{
    var indice = Math.floor(Math.random() * (vocalesRestantes.length - 1))
    var voc = vocalesRestantes[indice]
    vocalesRestantes.splice(indice,1)
    return voc
}

function getLetra()
{
    //Si las consonantes restantes o las vocales restantes estan vacias es que es el primer inicio y hay
    //que inicializar los arrays
        //console.log("Consonantes ==> Iniciales: " + consonantesIniciales.length + " - restantes: "+consonantesRestantes.length)
        //console.log("Vocales ==> Iniciales: " + vocalesIniciales.length + " - restantes: "+vocalesRestantes.length)

    if ((vocalesRestantes.length === 0) || (consonantesRestantes.length === 0))
        iniciarArrays();

    //50% de posibilidades de obtener una vocal o una consonante
    var salida = '';
    var random = Math.random()*2
    var floor = Math.floor(random)
        //console.log("RANDOM = " + random + " FLOOR " + floor)

//    console.log("numConsonantes = "+numConsonantes)
//    console.log("numVocales = "+numVocales)
    //Limitamos las posibles vocales o consonantes
    //por cada dos consonantes o vocales sacadas, sacamos la contraria
    if (numConsonantes - numVocales > 1)
    {
        //console.log("hay "+(numConsonantes - numVocales)+" consonantes mas que vocales, sacamos vocal")
        floor = 0
    }
    else if (numVocales - numConsonantes > 1)
    {
        //console.log("hay "+(numVocales - numConsonantes)+" vocales mas que consonantes, sacamos consonante")
        floor = 1
    }

    switch(floor)
    {
    case 0: salida = getVocal()
        numVocales++
        break
    default: salida = getConsonante()
        numConsonantes++
        break
    }

    return salida
}

//Iniciar / Inicializar arrays - Reiniciar partida
function iniciarArrays()
{
    console.log("Letras::iniciarArrays()")
    numConsonantes = 0
    numVocales = 0
    vocalesRestantes = []
    consonantesRestantes = []
    letrasPartida = []
    movimientos = []
    for (var i=0; i<vocalesIniciales.length; i++)
        vocalesRestantes.push(vocalesIniciales[i])
    for (var j=0; j<consonantesIniciales.length; j++)
        consonantesRestantes.push(consonantesIniciales[j])
}

// -- Añadir o eliminar movimientos ---------------------
function pushMovimiento(indiceEnLetras)
{
    //console.log("Añadir MOVIMIENTO ficha "+ indiceEnLetras)
    movimientos.push(indiceEnLetras)
}

function popMovimiento()
{
    return movimientos.pop()
}

function pushLetrasPartida(letra)
{
    letrasPartida.push(letra)
    console.log("AÑADIMOS la letra: "+letra)
    if (letrasPartida.length == 9)
        console.log("== LETRAS PARTIDA: "+letrasPartida+" ==")
}

function resetMovimientos()
{
    console.log("Letras::resetMovimientos()")
    movimientos = []
}

function reiniciar()
{
    console.log("Letras::reiniciar()")
    iniciarArrays()
}

function generaPartida()
{
    reiniciar()
    var salida = ""
    for (var i=0;i<9;i++)
        letrasPartida.push(getLetra())

    console.log("Partida generada con las letras: "+letrasPartida)
    console.log("Letraspartida.lenght = "+letrasPartida.length)
    return letrasPartida
}

function setPartida(datosPartida)
{
    reiniciar()
    console.log("DatosPartida: "+datosPartida)
    for (var i=0;i<9;i++)
//        pushLetrasPartida(datosPartida.substring(i,i+1))
        letrasPartida.push(datosPartida.substring(i,i+1))
    console.log("PARTIDA establecida con las letras: "+letrasPartida)
    console.log("letrasPartida.length= "+letrasPartida.length)
}

function getLetraPartida()
{
    var salida = ''
    console.log("getLetraPartida length:"+letrasPartida.length)
    if(letrasPartida.length > 0)
        salida = letrasPartida.pop()

    console.log("getLetraPartida = "+salida)
    return salida
}
