.pragma library


var textoAyudaRondaRapidaCifras = "<b>Objetivo:</b><br><br>El objetivo es obtener un número elegido al azar entre el 101 y el 999 \
con las operaciones aritméticas elementales (+,−,×,÷) con seis números (del 1 al 10, 25, 50, 75 y 100). No tienes porqué \
usar todos los números, pero no se puede repetir ninguno.  El tiempo para encontrar el exacto es de 60 segundos, aunque <b>podemos \
rendirnos antes, pulsando sobre el círculo del tiempo y una vez haya aparecido una bandera blanca, volviendo a pulsar</b>. Si no se \
pulsa por segunda vez, rápidamente volverá a mostrar los segundos restantes. <br><br>\
<b>Mecánica de Juego</b>:<br><br>Para realizar las operaciones, elegiremos un número pulsando sobre éĺ (el cuadro del número se pondrá azul), \
a continuación seleccionamos la operación que deseamos realizar y a continuación el siguiente operando. Si la operación se puede \
realizar, se creará un nuevo numero que contenga el resultado de la operación. Por ejemplo, seleccionamos un 6, posteriormente \
la operación multiplicar (X) y a continuacion el ultimo operando, en este caso 3. El resultado será un nuevo cuadro con el resultado de \
la operación, es decir, 18.<br>En el cuadro inferior se mostrará el número más cercano al exacto que hayamos encontrado automáticamente sin necesidad \
de pulsar ningún botón ni hacer nada. Si encontramos el exacto, la partida acaba.<br><br><b>Casos especiales para restas</b>:<br><br>   - Si intentamos restar a un \
número, otro mayor, el resultado será la resta del mayor menos el menor, \
por lo tanto, todas las operaciones de resta se pueden realizar.<br>   - Si restamos de un numero otro del mismo valor, el resultado será cero y el número \
por tanto desaparecerá.<br><br><b>Casos especiales para divisiones</b>:<br><br>   - Si intentamos dividir a un número entre otro mayor, y a su vez \
el primero es divisor del segundo, ocurrirá como con la resta y se dividirá el mayor de los dos numeros entre el menor.<br>   - Si intentamos dividir un número entre otro \
del que no es divisor ni múltiplo, los numeros se pondran rojos un segundo para mostrar el error y se quitará la selección de ambos. <br><br>\
<b>Botones</b>:<br><br>  'Deshacer': Pulsando a este botón desharemos el último movimiento realizado<br>  'Comenzar': Eliminamos todos los movimientos realizados \
y comenzamos de nuevo las operaciones"

var tituloAyudaRondaRapidaCifras = "Partida rápida de Cifras"
var textoAyudaRondaRapidaLetras = "<b>Objetivo</b>:  <br><br>El objetivo es encontrar la palabra más larga posible sin usar ninguna letra más de una vez. \
Son válidas las palabras recogidas en el Diccionario de la Real Academia Española. No son válidos los plurales.\ No son válidas \
las formas personales del verbo. Si están permitidas las formas no personales (infinitivo, participio y gerundio) de los verbos. El tiempo para encontrar la palabra es de 60 segundos, aunque <b>podemos \
rendirnos antes, pulsando sobre el círculo del tiempo y una vez haya aparecido una bandera blanca, volviendo a pulsar</b>. Si no se \
pulsa por segunda vez, rápidamente volverá a mostrar los segundos restantes.<br><br>\
<b>Mecánica de Juego</b>:   <br><br>Al ir pulsando sobre las letras obtenidas aleatoriamente, se irá formando debajo una nueva palabra. Si la palabra existe, las letras de la nueva \
palabra formada serán verdes, si no existe, serán rojas. Según vayamos descubriendo palabras de mayor longitud, en la parte inferior de la pantalla, se almacenará la palabra más \
larga encontrada y a su derecha habrá un círculo con el número de letras de la misma. <br>Para incluir o quitar letras a la palabra formada, podemos, o bien pulsar sobre la letra en \
el cuadro superior, o bien, podemos actuar con los botones que aparecerán cuando al menos haya una letra de la parte superior seleccionada.<br><br>\
<b>Botones</b>:<br><br>  'Deshacer': Pulsando a este botón desharemos el último movimiento realizado<br>  'Comenzar': Eliminamos todos los movimientos realizados \
y comenzamos de nuevo a buscar una palabra.<br>  'Mezclar': Mueve las letras para cambiar el punto de vista."

var tituloAyudaRondaRapidaLetras = "Partida rápida de Letras"
var textoAyudaPartidaCompleta = "La partida clásica es un serie de <b>10 rondas</b> que se reparten entre cifras y letras de la siguiente manera: <br><br> - Cifras, letras, letras, cifras, \
letras, letras, cifras, letras, letras y cifras. <br><br><b>Se trata de conseguir el mayor número de puntos sumando todas las rondas</b>. El tiempo para cada ronda es de 45 segundos, aunque <b>podemos \
rendirnos antes, pulsando sobre el círculo del tiempo y una vez haya aparecido una bandera blanca, volviendo a pulsar</b>. Si no se \
pulsa por segunda vez, rápidamente volverá a mostrar los segundos restantes. <br><br>En la parte inferior de la pantalla, se encuentra un marcador \
que va mostrando si la ronda ha sido de cifras o letras y los puntos logrados en cada una de ellas. Los puntos se reparten de la siguiente manera:<br><br><b>Reparto \
de puntos</b>: <br><br>   <b>Letras</b>:<br> - Un punto por cada letra de la palabra encontrada.<br><br>   <b>Cifras</b>: <br>  - 8 puntos por el exacto.<br>  - 6 puntos si te quedas cómo máximo a 2 de diferencia.<br>  - 5 puntos si te quedas a \
entre 3 y 5 puntos.<br>  - 4 puntos si te quedas como máximo a 10 de diferencia.<br>  - 3 puntos hasta 20.<br>  - 2 puntos hasta 50.<br>  - 1 punto si te quedas como máximo a 100 de diferencia.<br>  - 0 puntos si te quedas a más de \
100 de diferencia<br><br><b>Finalización de la partida</b>: <br><br>- Una vez acabada la partida, si el resultado entra en el ranking de récords (entre los 3 mejores), <b>debemos introducir el nombre \
del jugador con un máximo de ocho caracteres ya que de lo contrario no se guardará el resultado</b>. Sólo se puntúa en el ranking de récords con la partida clásica."

var tituloAyudaPartidaCompleta = "Partida Clásica"
var textoAyudaRecords = "Esta pantalla muestra el ranking con los tres mejores resultados logrados en una partida clásica. El resultado tiene que haber sido previamente guardado \
desde el aviso de partida finalizada en una partida clásica.<br><br><b> La máxima puntuación que se puede obtener es 86 </b>(32 puntos de cifras y 54 de letras)."
var tituloAyudaRecords = "Récords"

function getTextoAyuda(pantalla)
{
    var salida = ""
    switch (pantalla)
    {
        case 1: salida = textoAyudaRondaRapidaCifras
            break
        case 2: salida = textoAyudaRondaRapidaLetras
            break
        case 3: salida = textoAyudaPartidaCompleta
            break
        case 4: salida = textoAyudaRecords
            break
        default:
            break
    }
    console.log("Ayuda.getTextoAyuda("+pantalla+")")
    return salida
}

function getTituloAyuda(pantalla)
{
    var salida = ""
    switch (pantalla)
    {
        case 1: salida = tituloAyudaRondaRapidaCifras
            break
        case 2: salida = tituloAyudaRondaRapidaLetras
            break
        case 3: salida = tituloAyudaPartidaCompleta
            break
        case 4: salida = tituloAyudaRecords
            break
        default:
            break
    }
    console.log("Ayuda.getTituloAyuda("+pantalla+")")
    return salida
}
