import QtQuick 2.3
import QtQuick.Controls 1.2
//import QtGraphicalEffects 1.0

Item {

    id: root
    signal cambiarQml (string url)  //SEÑAL COMPARTIDA por todas las pantallas

    Rectangle {
        id: fondo
        anchors.fill: parent
        color: "#333333"
    }

// -- TITULO ----- 18% ---------------------------------------------------------------

    Item {
        id: titulo
        anchors.top: root.top
        anchors.horizontalCenter: parent.horizontalCenter
        height: root.height * 0.18
        width: root.width

    }

    Image {
        id: logo
        source: "qrc:/images/logoapp.png"
        anchors.top: root.top
        anchors.topMargin: root.height*0.05
        anchors.horizontalCenter: root.horizontalCenter
        anchors.bottomMargin: root.height*0.03
        //Premium
        //height: root.height*0.20
        //Free
        height: root.height*0.22
        width: root.width
        fillMode: Image.PreserveAspectFit
    }

// -- VISTA CENTRAL 82% ------------------------------------------------------------------

    GridView {

        id: vistaCentral
        anchors.top: logo.bottom
        anchors.topMargin: root.height*0.02
        anchors.horizontalCenter: root.horizontalCenter
        width: root.width*0.95
        height: root.height*0.78
        model: ModeloMenuPrincipal{}
        cellWidth: width/2
        cellHeight: height/ 3
        interactive: false // no flickable

        property bool activo: false

        delegate: DelegateMenuPrincipal {

                    height: vistaCentral.cellHeight
                    width: vistaCentral.cellWidth
                    urlQml: qml
                    urlImagen: fuenteImagen
                    textoBoton: texto

                    onUpdateLoader: root.cambiarQml(url)
                }
    }

    // Botonera 20%

    Item {
        id: botonera
        height: root.height*0.2
        width: root.width*0.9
        anchors.horizontalCenter: root.horizontalCenter
        anchors.bottom: root.bottom


        BotonImagen {
            id: salir
            height: parent.height*0.6
            width: height
            anchors.right: parent.right
            anchors.verticalCenter: parent.verticalCenter
            imagePath: "qrc:/images/images/salir.png"
            onPulsado: receiver.salirDelJuego()
        }

        BotonImagen {
            id: info
            height: parent.height*0.4
            width: height
            anchors.verticalCenter: parent.verticalCenter
            anchors.left: parent.left
            imagePath: "qrc:/images/images/info.png"
            onPulsado: {
                if (dialogoInfo.visible === true)
                    dialogoInfo.visible = false
                else
                    dialogoInfo.visible = true
            }
        }


        BotonImagen {
            id: botonEnglishVersion
            width: parent.height*0.30
            height: width
            imagePath: "qrc:/images/images/ppolicy.png"
            anchors.centerIn: parent
            onPulsado:  receiver.abrirNavegador("https://dres84.github.io")
        }

        Info {
            id: dialogoInfo
            visible: false
            anchors.left: info.right
            anchors.leftMargin: height*0.15
            anchors.right: salir.left
            anchors.rightMargin: height*0.15
            height: parent.height*0.5
            anchors.verticalCenter: info.verticalCenter
            onVisibleChanged: {
                if (visible)
                {
                    botonEnglishVersion.enabled = false
                    info.imagePath = "qrc:/images/images/cerrar.png"
                }
                else
                {
                    botonEnglishVersion.enabled = true
                    info.imagePath = "qrc:/images/images/info.png"
                }
            }
        }
    }
}
