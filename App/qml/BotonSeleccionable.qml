import QtQuick 2.3
import QtQuick.Controls 1.2

Rectangle {

    id: root
    property string imagePath: img.source
    property string name
    property bool activo: false
    property bool seleccionado : false
    signal pulsado
    border.color: "transparent"
    color: "transparent"
    border.width: 2

    Image
    {
        id: img
        anchors.fill: parent
        source: root.imagePath
        opacity:  (mouseArea.containsMouse && activo) ? 0.8 : 1.0
    }

    MouseArea {

        id: mouseArea
        anchors.fill: parent
        hoverEnabled: true
        onClicked: {
            //console.log("Boton seleccionable pulsado: " + name + " - activo = " + activo);
            root.pulsado();
        }
    }

    onSeleccionadoChanged:
    {
        if (seleccionado)
            root.border.color = "#FFFFFF";
        else
            root.border.color = "transparent";
    }
}
