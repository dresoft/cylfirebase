import QtQuick 2.3
import QtQuick.Controls 1.2

import "../javascript/Textos.js" as Textos

//BOTONES de las operaciones:
//deshacer, reiniciar y mezclar

//PULSADA
//Operaciones: "UNDO" "MIX" "RESTART"

Rectangle {

    id: root
    color: "transparent"

    property bool activo: true
    property bool deshacerActivo: false
    property bool reiniciarActivo: false
    property bool mezclarActivo: false

    signal pulsada (string op)

    //Boton DESHACER -----33% Ancho-------------------------------------------
    Rectangle
    {
        id: botonDeshacer

        width: parent.width / 3
        height: parent.height
        anchors.left: parent.left
        anchors.verticalCenter: parent.verticalCenter
        color: "transparent"
        visible: deshacerActivo

        Image {
            source: "qrc:/images/images/deshacer.png"
            width: parent.height*0.8
            height:width
            anchors.top: parent.top
            anchors.horizontalCenter: parent.horizontalCenter
        }

        Text {
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.bottom: parent.bottom
            height: parent.height*0.2
            width: parent.width
            text: "Deshacer"
            color: "white"
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            font.pixelSize: parent.height/5
        }

        MouseArea
        {
            anchors.fill: parent
            onClicked: pulsada("UNDO");
        }
    }

    //Boton MEZCLAR -----33% Ancho-----------------------------------------------
    Rectangle
    {
        id: botonMezclar

        width: parent.width / 3
        height: parent.height
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.verticalCenter: parent.verticalCenter
        color: "transparent"
        visible: mezclarActivo

        Image {
            source: "qrc:/images/images/shuffle.png"
            width: parent.height*0.8
            height:width
            anchors.top: parent.top
            anchors.horizontalCenter: parent.horizontalCenter
        }

        Text {
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.bottom: parent.bottom
            height: parent.height*0.2
            width: parent.width
            text: "Mezclar"
            color: "white"
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            font.pixelSize: parent.height/5
        }

        MouseArea
        {
            anchors.fill: parent
            onClicked: pulsada("MIX");
        }
    }

    //Boton REINICIAR -----33% Ancho--------------------------------------
    Rectangle
    {
        id: botonReiniciar

        width: parent.width / 3
        height: parent.height
        anchors.right: parent.right
        anchors.verticalCenter: parent.verticalCenter
        color: "transparent"
        visible: reiniciarActivo

        Image {
            source: "qrc:/images/images/reiniciar.png"
            width: parent.height*0.8
            height:width
            anchors.top: parent.top
            anchors.horizontalCenter: parent.horizontalCenter
        }

        Text {
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.bottom: parent.bottom
            height: parent.height*0.2
            width: parent.width
            text: "Comenzar"
            color: "white"
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            font.pixelSize: parent.height/5
        }

        MouseArea
        {
            anchors.fill: parent
            onClicked: pulsada("RESTART");
        }
    }

// -- SLOTS --------------------------------------------------------------

    function mostrarTodo()
    {
        deshacerActivo = true
        mezclarActivo = true
        reiniciarActivo = true
    }

    function ocultarTodo()
    {
        deshacerActivo = false
        mezclarActivo = false
        reiniciarActivo = false
    }

    function mostrarSoloMezclar()
    {
        deshacerActivo = false
        mezclarActivo = true
        reiniciarActivo = false
    }

    function mostrarDeshacerYReiniciar()
    {
        deshacerActivo = true
        mezclarActivo = false
        reiniciarActivo = true
    }
}
