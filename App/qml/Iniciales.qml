import QtQuick 2.0

import "../javascript/Records.js" as Records
import "../javascript/PartidaCompleta.js" as Partida

Item {
    id: iniciales
    signal tecladoAb
    signal tecladoCe

    Rectangle {
        id: fondo
        width: parent.width
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.verticalCenter: parent.verticalCenter
        height: parent.height*0.3
        color: "#fbfbd0"
        radius: 4

        Text {
            id: texto
            anchors.fill: parent
            font.pixelSize: height*0.4
            text: "Pulsa para introducir tu nombre"
            style: Text.Sunken
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            font.family: "Ubuntu"
        }

        TextInput {

            id: nombre
            anchors.fill: parent
            font.pixelSize: height*0.5
            font.family: "Ubuntu"

            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            wrapMode: Text.WordWrap
            maximumLength: 8

            onFocusChanged: {

                console.log("onFocusChanged")
                texto.visible = false
                text = "";
                tecladoAb();
            }
            onAccepted: {
                var textoFinal = receiver.eliminarCaracteresPeligrosos(text)
                if (textoFinal === "") textoFinal = "anónimo"
                texto.text = "Récord de <b>"+textoFinal+"</b> guardado"
                texto.visible = true

                //guardamos los records en memoria y luego en disco
                Records.guardarRecord(Partida.puntuacionTotal,textoFinal)
                guardarRecordsEnDisco()

                nombre.visible = false
                tecladoCe()
            }
        }
    }

    function guardarRecordsEnDisco()
    {
        receiver.setpuntosOro(Records.puntuacionOro)
        receiver.setpuntosPlata(Records.puntuacionPlata)
        receiver.setpuntosBronce(Records.puntuacionBronce)
        receiver.setNombreOro(Records.nombreOro)
        receiver.setNombrePlata(Records.nombrePlata)
        receiver.setNombreBronce(Records.nombreBronce)
        receiver.guardarRecords()
    }

    //realizamos esta operacion por seguridad para evitar inyecciones de codigo
    function quitarCaracteresInvalidos(texto)
    {
        var cadena = "";
        console.log("textAntes = " + texto)
        cadena = texto
        cadena.replace(/;/gi,"")
        console.log("textDespues1 = " + texto)
        cadena.replace(/\n/gi,"")
        console.log("textDespues2 = " + texto)
        return cadena
    }
}

