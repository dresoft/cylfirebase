import QtQuick 2.0
import QtWebSockets 1.0

Item {

    property bool activo: false
    property string texto: messageBox.text
    property bool conectado: false

//    WebSocket {
//        id: socket
//        url: "ws://a0e24083.ngrok.io/socket.io/?EIO=3&transport=websocket"
//        onTextMessageReceived: {
//            messageBox.text = message
//        }
//        onStatusChanged: if (socket.status == WebSocket.Error) {
//                             console.log("Error: " + socket.errorString)
//                             conectado = false
//                         } else if (socket.status == WebSocket.Open) {
//                             socket.sendTextMessage("Hello World")
//                             conectado = true
//                         } else if (socket.status == WebSocket.Closed) {
//                             messageBox.text += "\nSocket closed"
//                             conectado = false
//                         }
//        active: activo
//    }

    WebSocket {

        id: secureWebSocket
        url: "wss://a0e24083.ngrok.io/socket.io/?EIO=3&transport=websocket"
        active: activo
        onTextMessageReceived: {
            messageBox.text = message
            sendTextMessage("CARADECULO")
        }
        onStatusChanged: {
                        sendTextMessage("CARADECULO")

                        console.log("ESTADO CAMBIADO = "+(socket.errorString))
                        if (socket.status === WebSocket.Error) {
                             console.error("Error: " + socket.errorString)
                        } else if (socket.status === WebSocket.Closed) {
                             console.log("WebSocket closed")
                             messageBox.text += "\nSocket closed"
                        } else if (socket.status === WebSocket.Open) {
                             console.log("WebSocket open")
                        } else if (socket.status === WebSocket.Connecting) {
                            console.log("WebSocket Connecting")
                        } else if (socket.status === WebSocket.Closing) {
                            console.log("WebSocket Closing")
                        }
        }

    }
    Text {
        id: messageBox
        text: secureWebSocket.status === WebSocket.Open ? qsTr("Creando sala...") : qsTr("Sala creada!")
        //anchors.centerIn: parent
    }

}
