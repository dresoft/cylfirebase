import QtQuick 2.3
import QtQuick.Controls 1.2

//Valores de las operaciones:
//SUMA 1 RESTA 2 MULPLICACION 3 DIVISION 4

Rectangle {

    id: root
    radius: 40
    smooth: true
    color: "transparent"

    property bool activo: false
    property int espaciado: width/15
    property bool haySeleccionada : false
    property int operSeleccionada : -1
    signal cambioOperacionSeleccionada (bool selected,int numOper)


    BotonSeleccionable {
        id: botonSuma

        imagePath: "qrc:/images/images/sumaFondoGranate.png"
        width: parent.width / 5
        height: width
        anchors.left: parent.left
        anchors.verticalCenter: parent.verticalCenter
        onPulsado: manejaSeleccion(1);
    }

    BotonSeleccionable {
        id: botonResta

        imagePath: "qrc:/images/images/restaFondoAzul.png"
        width: parent.width / 5
        height: width
        anchors.left: botonSuma.right
        anchors.verticalCenter: parent.verticalCenter
        anchors.leftMargin: espaciado

        onPulsado: manejaSeleccion(2);
    }

    BotonSeleccionable {
        id: botonMultiplicacion

        name: "boton multiplicacion"
        imagePath: "qrc:/images/images/multiplicacionFondoGranate.png"
        width: parent.width / 5
        height: width
        anchors.left: botonResta.right
        anchors.verticalCenter: parent.verticalCenter
        anchors.leftMargin: espaciado

        onPulsado: manejaSeleccion(3);
    }

    BotonSeleccionable {
        id: botonDivision

        name: "boton division"
        imagePath: "qrc:/images/images/divisionFondoAzul.png"
        width: parent.width / 5
        height: width
        anchors.left: botonMultiplicacion.right
        anchors.leftMargin: espaciado
        anchors.verticalCenter: parent.verticalCenter

        onPulsado: manejaSeleccion(4);
    }


// -- SLOTS --------------------------------------------------------------

    onActivoChanged:
        if (!activo)
        {
            deseleccionarTodo();
            desactivaTodas();
        }
        else
            activaTodas();


// -- Funciones ----------------------------------------------------------

    function activaTodas ()
    {
        botonSuma.activo = true;
        botonResta.activo = true;
        botonMultiplicacion.activo = true;
        botonDivision.activo = true;
    }

    function desactivaTodas ()
    {
        botonSuma.activo = false;
        botonResta.activo = false;
        botonMultiplicacion.activo = false;
        botonDivision.activo = false;
    }

    function deseleccionarTodo()
    {
        botonSuma.seleccionado = false;
        botonResta.seleccionado = false;
        botonMultiplicacion.seleccionado = false;
        botonDivision.seleccionado = false;
        haySeleccionada = false;
        operSeleccionada = -1;
    }

    //Funcion que se encarga de seleccionar o deseleccionar elementos segun
    //el criterio de que solo pueden ser seleccionadas cuando esten activos y
    //que solo puede haber un elemento seleccionado simultaneamente
    function manejaSeleccion (oper)
    {
        if (activo)
            seleccionaOper(oper);
        else
            deseleccionarTodo();
     }

    //Funcion que deselecciona todas , y selecciona la que queramos
    function seleccionaOper(oper)
    {
        //Si ya estaba seleccionada
        if (oper === operSeleccionada)
        {
            //Deseleccionamos todos y salimosl
            deseleccionarTodo();
            return;
        }

        //Antes de seleccionar deseleccionamos todas
        deseleccionarTodo();
        switch (oper)
        {
            case 1:
                botonSuma.seleccionado = true;
                break;
            case 2:
                botonResta.seleccionado = true;
                break;
            case 3:
                botonMultiplicacion.seleccionado = true;
                break;
            case 4:
                botonDivision.seleccionado = true;
                break;
            default:
                break;

        }

        //no cambiar orden de estas 2 sentencias ya que actuamos segun el cambio en haySeleccionada
        operSeleccionada = oper;
        haySeleccionada = true;

    }

    onHaySeleccionadaChanged:
        cambioOperacionSeleccionada(haySeleccionada,operSeleccionada);


}
