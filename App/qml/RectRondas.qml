import QtQuick 2.0

Rectangle {

    id: root
    border.width: 2
    border.color: "black"
    height: parent.height
    width: parent.width*0.1
    color: "lightgray"

    opacity: 0.2

    property bool activo: false
    property int puntuacion
    property int ronda
    property string juego
//    property string colorFondo: root.color

    onActivoChanged: pintar()

    Text {
        id: txtTipoJuego
        height: row.height/2
        anchors.top: root.top
        anchors.horizontalCenter: parent.horizontalCenter
        text: juego
        font.bold: true
        horizontalAlignment: Text.AlignHCenter
        color: (juego === "C") ? "blue" : "red"
    }
    Text {
        id: txtPuntuacion
        height: row.height/2
        anchors.top: txtTipoJuego.bottom
        anchors.horizontalCenter: parent.horizontalCenter
        text: (puntuacion === -1) ? "-" : puntuacion
        horizontalAlignment: Text.AlignHCenter
        font.bold: true
    }

    function pintar()
    {
        if (activo)
        {
           root.opacity = 1
           root.border.color = (juego === "C") ? "blue" : "red"
           root.color = "white"
        }
        else
        {
            root.border.color = "black"
            root.color = "lightgray"
        }
    }

    function activar()
    {
        if (activo)
            activo = false
        else
            activo = true
    }
}
