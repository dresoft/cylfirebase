import QtQuick 2.0
import QtQuick.Controls 2.1
//import QtQuick.Controls.Styles 1.4

Item {

    id: pantallaCon
    width: parent.width * 0.8
    height: parent.height * 0.5
    property bool activado: false

    Rectangle {
        id: fondo
        anchors.fill: parent
        color: "green"
        border.color: "white"
        border.width: 3
        radius: 5
    }

    Text {
        id: textoPantalla
        width: parent.width
        height: parent.height*0.2
        anchors.top: parent.top
        anchors.topMargin: height*0.3
        anchors.bottomMargin: height*0.3
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
        text:  "Creando sala..."
        font.family: "Monaco"
        font.bold: true
        wrapMode: Text.WordWrap
        color: "white"
        font.pixelSize: height*0.3
    }

    Item {
        id: parteInferior
        anchors.top: textoPantalla.bottom
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: parent.bottom

        BusyIndicator {

            id: indicador
            height: parent.height*0.7
            width: height
            anchors.centerIn: parent
            visible: true
        }

        Text {
            anchors.fill: indicador
            visible: !indicador.visible
            text: socket.texto
        }
    }

    Socket {
        id: socket
        activo: pantallaCon.visible
        onConectadoChanged: if (conectado)
                            {
                                indicador.running = false
                                indicador.visible = false
                            }
        onTextoChanged: console.log(texto)
    }


    Component.onCompleted: indicador.running = true
}
