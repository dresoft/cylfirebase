import QtQuick 2.0

Item {

    id: info

    Rectangle {
        id: fondo
        anchors.horizontalCenter: parent.horizontalCenter
        height: parent.height
        width: parent.width*0.8
        color: "lightblue"
        radius: 4
    }

    Text {
        id: texto
        height: parent.height
        width: parent.width*0.6
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: parent.top
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
        wrapMode: Text.WordWrap
        font.pixelSize: height*0.18
        elide: Text.ElideLeft
        color: "brown"
        //text: "C&L 1.43 Premium\nDesarrollado por: \n= dreSoft =\nSíguenos en Twitter"
        text: "C&L 1.45 Free\nDesarrollado por: \n= dreSoft =\nSíguenos en Twitter"
    }

    BotonImagen {
        id: mail
        anchors.left: parent.left
        anchors.verticalCenter: parent.verticalCenter
        width: info.width*0.2
        height: width
        imagePath: "qrc:/images/images/mail.png"
        onPulsado: {
            receiver.enviarMail()
        }
    }

    BotonImagen {
        id: twitter
        anchors.right: parent.right
        anchors.verticalCenter: parent.verticalCenter
        width: info.width*0.2
        height: width
        imagePath: "qrc:/images/images/twitter.png"
        onPulsado: {
            receiver.abrirNavegador("https://twitter.com/dreSoft_")
        }
    }

}

