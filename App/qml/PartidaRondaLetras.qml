// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 2.3
import QtQuick.Controls 1.2

import "../javascript/Letras.js" as Letras
import "../javascript/ConfiguracionGeneral.js" as Conf
import "../javascript/PartidaCompleta.js" as Partida

Item {

    id: root

    property int numeroDeFicha: 0  //numero ordinal de letra q se esta sacando, comienza en 0
    property int numeroDeLetrasPalabra: modeloPalabra.count
    property int valorInicialCuentaAtras: Conf.tiempoRonda //tiempo en segundos de la cuenta atras

    property string palabra: "" //palabra que va apareciendo
    property string palabraMasLarga //palabra mas larga que sera mostrada en palabraMasLarga
    property string letraTemp

    property bool todasSacadas: false //se han sacado todas las letras?
    property bool partidaPausada: false
    property bool partidaTerminada: false
    property bool empezarEnPausa: false

    property bool insertarMov: true //Flag para evitar sumar movimientos al simular clicks sobre las cifras
    property bool deseleccionando: false //flag para evitar que al deseleccionarTodas() encuentre una palabra

    property int tamañoFuenteMediana: height*0.025
    property int tamañoFuenteTitulo: height*0.030

    signal cambiarQml (string url)  //SEÑAL COMPARTIDA por todas las pantallas
    signal ayuda (int idPantalla)   //SEÑAL COMPARTIDA por todas las pantallas
    signal continuar(int puntuacion)
    signal reiniciarPartidaCompleta
    signal mostrarDiccionario(string palabraABuscar)

    Rectangle {
        id: fondo
        anchors.fill: parent
        color: "#333333"
    }

 // -- ZONA SUPERIOR DE LA PANTALLA -----------------------------------------------

    CuentaAtras
    {
        id: crono
        activo: false

        height: root.height/9
        width: height
        anchors.top: root.top
        anchors.right: root.right
        anchors.rightMargin: root.width/20
        anchors.topMargin: root.height/30
        valorInicialSegundos: valorInicialCuentaAtras

        onRunningChanged:{

        }

        onTiempoAgotado: {
            acabarPartida()
            console.log("=== TIEMPO AGOTADO ===")
            console.log("== Palabra mas larga -> "+palabraMasLarga+" ==")
            sobrefondo.visible = true
            textoRonda.text = "RONDA " + Partida.rondaActual
            palabraDe9NoEncontrada.visible = true
        }
    }

    Rectangle {
        id: fondoGridLetras
        anchors.fill: gridLetras
        radius: 4
        color: "gray"
        visible: gridLetras.visible ? true : false
    }

    //Grid con letras pulsables
    GridView {
        id: gridLetras

        model: modeloLetras
        width:  root.width*0.9
        height: width*0.6
        anchors.top: crono.bottom
        anchors.topMargin: height/10
        anchors.horizontalCenter: parent.horizontalCenter
        cellWidth: width/5
        cellHeight: height/2

        interactive: false //no flickable

        property bool activo: false

        delegate: Letra {
                        height: gridLetras.cellHeight
                        width: gridLetras.cellWidth
                        text: valor
                        indice: index
                        usada: false
                        visible: (valor == '') ? false : true
                        activo: gridLetras.activo //se activara cuando la GridView este activa
                        onUsadaChanged: {
                            if (usada)
                            {
                                indiceEnPalabra = numeroDeLetrasPalabra
                                addItemModeloPalabra(indice)
                            }
                            else
                            {
                                delItemModeloPalabra(indiceEnPalabra)
                                indiceEnPalabra = -1
                            }
                        }
                    }
    }

    //GridView con la palabra que se va formando
    GridView {
        id: gridPalabra

        model: modeloPalabra
        width:  gridLetras.width
        height: gridLetras.height/3.5
        anchors.top: gridLetras.bottom
        anchors.topMargin: height/2
        anchors.horizontalCenter: root.horizontalCenter
        cellWidth: width/9
        cellHeight: height

        interactive: false //no flickable

        property bool activo: false
        property string colorTexto: "black"

        delegate: LetraPequena {
                        height: gridPalabra.cellHeight
                        width: gridPalabra.cellWidth
                        text: valor
                        indiceLetraPequena: indexLetraPequena
                        indiceLetraGrande: indexLetraGrande
                        colorTexto: gridPalabra.colorTexto
                  }
    }

 // -- botonera LETRAS (CONTROLA LOS MOVIMIENTOS) -- 15% Alto -----------------------------------------------

    BotoneraLetras {
        id: botonera
        width: gridPalabra.width
        height: root.height*0.1
        anchors.top: gridPalabra.bottom
        anchors.topMargin: height/2
        anchors.horizontalCenter: root.horizontalCenter
        activo: true

        onPulsada: {
            if (op == "UNDO")
            {
                console.log("-- UNDO --")
                deshacerMovimiento()
            }
            else if (op == "RESTART")
            {
                console.log("-- RESTART --")
                cargarMovimientoInicial()
            }
            else if (op == "MIX")
            {
                console.log("-- MIX --")
                mezclarLetras()
            }
        }
    }

    BotonImagen {
        id: botonPlay
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: botonera.top
        visible: false
        height: parent.height*0.14
        width: height
        imagePath: "qrc:/images/images/play.png"
        onPulsado: {
            pausarPartida()
        }
    }

    // -- FUNCIONES BOTONERA LETRAS ----------------------------------------------------
        function deshacerMovimiento()
        {
            console.log("deshacerMovimiento("+Letras.movimientos.length+")")
            if (Letras.movimientos.length < 1)
                return

            //indicamos que este movimiento no se añadira al array de movimientos
            insertarMov = false

            var movimiento = Letras.popMovimiento()
            gridLetras.currentIndex = movimiento
            gridLetras.currentItem.clicked()

            //Si es 0 despues del ultimo pop desactivamos la botonera
            if (Letras.movimientos.length == 0)
            {
                botonera.reiniciarActivo = false
                botonera.deshacerActivo = false
                botonera.mezclarActivo = true
            }
        }

        function cargarMovimientoInicial()
        {
            //Eliminamos los movimientos y deseleccionamos todas las letras
            Letras.resetMovimientos()
            deseleccionando = true
            deseleccionarTodas()
            deseleccionando = false
            botonera.deshacerActivo = false
            botonera.reiniciarActivo = false
            botonera.mezclarActivo = true
        }

        function deseleccionarTodas()
        {
            //console.log("GRIDLETRAS.count = " + gridLetras.count)
            for (var i=0; i < gridLetras.count; i++)
            {
                //indicamos que este movimiento no se añadira al array de movimientos
                insertarMov = false
                gridLetras.currentIndex = i
                gridLetras.currentItem.usada = false
            }
            insertarMov = true
        }

        function mezclarLetras()
        {
            for(var i=0;i<30;i++)
            {
                var aleatorio = Math.floor(Math.random() * 9)
                var aleatorio2 = Math.floor(Math.random() * 9)
                var count = Math.floor(Math.random() * 2)+1
                //console.log(aleatorio+" - "+aleatorio2+" "+count)
                modeloLetras.move(aleatorio,aleatorio2,count)
            }
            //actualizamos los indices de las l
            for (var j=0;j<modeloLetras.count;j++)
            {
                gridLetras.currentIndex = j
                gridLetras.currentItem.indice = j
            }
            //console.log(i+": "+gridLetras.currentItem.text+" - Indice = "+gridLetras.currentItem.indice)
        }

 //  -- PARTE INFERIOR --10% Alto ------------------------------------------------

    //Numero mas cercano al exacto
     Item
     {
         id: rectanguloPalabraMasLarga
         height: root.height*0.1
         width: root.width*0.9
         anchors.horizontalCenter: root.horizontalCenter
         anchors.bottom: root.bottom
         anchors.bottomMargin: root.height*0.02

         Rectangle {

            id: fondoPalabraMasLarga
            width: parent.width * 0.85
            height: parent.height
            anchors.left: parent.left
            anchors.verticalCenter: parent.verticalCenter
            color: "transparent"
            radius: 4

            Text {
                id: textoPalabraMasLarga
                text: ""
                width: parent.width
                height: parent.height
                font.pixelSize: parent.height/1.5
                font.bold: true
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
                color: "white"
            }
        }

        Rectangle {
            id: sumatorioLetras

            radius: 400
            width: parent.width*0.15
            height: width
            color: "black"
            anchors.right: parent.right
            anchors.verticalCenter: parent.verticalCenter

            Text {
                id: textoSumatorioLetras

                anchors.centerIn: parent
                text: textoPalabraMasLarga.text.length
                font.pixelSize: sumatorioLetras.height*0.8
                color: "white"
                onTextChanged: {
                    switch(textoPalabraMasLarga.text.length)
                    {
                       case 0: sumatorioLetras.color = "#FF0000"; break
                       case 1: sumatorioLetras.color = "#DD2200"; break
                       case 2: sumatorioLetras.color = "#BB4400"; break
                       case 3: sumatorioLetras.color = "#996000"; break
                       case 4: sumatorioLetras.color = "#807000"; break
                       case 5: sumatorioLetras.color = "#708000"; break
                       case 6: sumatorioLetras.color = "#609900"; break
                       case 7: sumatorioLetras.color = "#44BB00"; break
                       case 8: sumatorioLetras.color = "#22DD00"; break
                       case 9: sumatorioLetras.color = "#00FF00"; break
                       default: break
                    }
                }
            }
        }
    }

    //temporizador para que el valor mas cercano vuelva a fondo blanco en 500 ms
    Timer {
        id: vuelveABlanco

        interval: 500
        running: false
        repeat: false
        onTriggered: {
            textoPalabraMasLarga.color = "white"
        }
    }

    onPalabraMasLargaChanged: {
        textoPalabraMasLarga.text = palabraMasLarga.toString()
        console.log("Nueva palabra mas larga: "+palabraMasLarga)
        //si el valor es el exacto
        if (palabraMasLarga.length == 9)
        {
            acabarPartida()
            console.log("Palabra de 9 letras encontrada! \""+palabra+"\"")
            console.log("=== PARAMOS CRONO ==")
            crono.arrancarPararCrono()
            sobrefondo.visible = true
            textoRondaDe9.text = "RONDA " + Partida.rondaActual
            palabraDe9Encontrada.visible = true
            botonera.visible = false
        }

        if (palabraMasLarga.length != 9)
        {
            textoPalabraMasLarga.color = "gold"
            vuelveABlanco.running = true
        }
    }


// -- OCULTADORES en PAUSA ------------------------------------------
    //Elementos que ocultan los gridViews cuando estamos en pausa

        Rectangle {
            anchors.fill: gridLetras
            color: "orangered"
            radius: 4
            visible: partidaPausada

            Text {
                anchors.centerIn: parent
                font.pixelSize: parent.height/3
                color: "white"
                text: "EN PAUSA"
                font.family: "Ubuntu Mono"
            }
        }

        Rectangle {
            anchors.fill: gridPalabra
            color: "orangered"
            radius: 4
            visible: partidaPausada
        }

// -- MENSAJES sobre la PANTALLA ------------------------------------------------

    //SOBREFONDO para oscurecer la pantalla
    Rectangle {
        id: sobrefondo

        anchors.fill: root
        visible: false
        color: "black"
        opacity: 0.7

        MouseArea {
            anchors.fill: parent
        }
    }

    //PALABRA DE 9 ENCONTRADA
    //Mensaje - Encabezado 0.2 - Centro 0.6 (2*0.3) - Boton Continuar 0.2
    Rectangle {
        id: palabraDe9Encontrada

        anchors.centerIn: root
        visible: false
        width: root.width/1.5
        height: root.height/2
        radius: 4
        color: "green"
        border.color: "white"
        border.width: 2

        //Ponemos el texto con la ronda terminada
        Rectangle {
            id: fondoRondaDe9
            anchors.bottom: parent.top
            anchors.horizontalCenter: parent.horizontalCenter
            height: parent.height*0.1
            width: parent.width*0.6
            color: "brown"
            radius: 4

            Text {
                id: textoRondaDe9
                anchors.fill: parent
                anchors.centerIn: parent
                text: ""
                font.family: "Ubuntu"
                color: "white"
                font.pixelSize: height*0.8
                font.bold: true
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
            }
        }

        Item {

            id: copaImagen
            height: parent.height*0.2
            width: height
            anchors.top: palabraDe9Encontrada.top
            anchors.left: palabraDe9Encontrada.left

            Image {
                id: img
                source: "qrc:/images/images/copa.png"
                anchors.centerIn: parent
                height: parent.width*0.8
                width: height
            }

            Text {
                id: textoPalabraDe9Encontrada
                anchors.verticalCenter: parent.verticalCenter
                anchors.left: parent.right
                height: parent.height/2
                text: "¡¡GUAU, UNA DE 9!!"
                color: "white"
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
                font.pixelSize: tamañoFuenteTitulo
                font.bold: true

            }
        }

        Text {
            id: palabraEncontradaJugadorDe9
            anchors.horizontalCenter: parent.horizontalCenter
            width: parent.width*0.9
            height: parent.height*0.25
            anchors.top: copaImagen.bottom
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            wrapMode: Text.Wrap
            text: "Has encontrado la palabra:"
            color: "white"
            font.pixelSize: tamañoFuenteMediana
            font.bold: true

            Rectangle {
                id: rectPalabraJugadorDe9
                width: parent.width*0.6
                height: parent.height/3
                color: "black"
                anchors.bottom: parent.bottom
                anchors.horizontalCenter: parent.horizontalCenter

                Text {
                    anchors.centerIn: parent
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter
                    color: "white"
                    text: palabraMasLarga
                    font.pixelSize: cronoImagen.height*0.3
                    font.bold: true
                }
            }

            BotonImagen {
                id: lanzarRAEJugadorDe9

                width: parent.height*0.55
                height: width
                imagePath: "qrc:/images/images/diccionario.png"
                anchors.top: rectPalabraJugadorDe9.top
                anchors.left: rectPalabraJugadorDe9.right
                anchors.leftMargin: width/5
                onPulsado:  mostrarDiccionario(palabraMasLarga)

                Text {
                    anchors.top: parent.bottom
                    anchors.horizontalCenter: parent.horizontalCenter
                    width: parent.width
                    text: "Dic. RAE"
                    font.pixelSize: width/4
                    color: "white"
                }
            }
        }

        Text {
            id: palabraEncontradaCerebroDe9
            anchors.horizontalCenter: parent.horizontalCenter
            width: parent.width*0.9
            height: parent.height*0.25
            anchors.top: palabraEncontradaJugadorDe9.bottom
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            wrapMode: Text.Wrap
            text: "Cerebro ha encontrado:"
            color: "white"
            font.pixelSize: tamañoFuenteMediana
            font.bold: true

            Rectangle {
                id: rectCerebroDe9
                width: parent.width*0.6
                height: parent.height/3
                color: "black"
                anchors.bottom: parent.bottom
                anchors.horizontalCenter: parent.horizontalCenter

                Text {
                    id: palabraMaximaConseguidaCerebroDe9
                    anchors.centerIn: parent
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter
                    color: "white"
                    text: ""
                    font.pixelSize: copaImagen.height*0.3
                    font.bold: true
                }
            }

            BotonImagen {
                id: lanzarRAEDe9

                width: parent.height*0.55
                height: width
                imagePath: "qrc:/images/images/diccionario.png"
                anchors.top: rectCerebroDe9.top
                anchors.left: rectCerebroDe9.right
                anchors.leftMargin: width/5
                onPulsado:  mostrarDiccionario(palabraMaximaConseguidaCerebro.text)

                Text {
                    anchors.top: parent.bottom
                    anchors.horizontalCenter: parent.horizontalCenter
                    width: parent.width
                    text: "Dic. RAE"
                    font.pixelSize: width/4
                    color: "white"
                }
            }
        }

        //FIJO EN TODOS LOS MENSAJES DE FIN DE PARTIDA (imagen y texto continuar partida) ----
        BotonImagen
        {
            id: btnContinuar
            name: "btnContinuar"
            height: parent.height*0.15
            width: height

            anchors.bottom: txtContinuar.top
            anchors.horizontalCenter: parent.horizontalCenter
            imagePath: "qrc:/images/images/siguiente.png"
            onPulsado: continuar(palabraMasLarga.length)
        }

        Text {

            id: txtContinuar
            text: "Continuar"
            height: parent.height*0.05
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.bottom: parent.bottom
            anchors.bottomMargin: btnContinuar.height/8
            color: "white"
            font.pixelSize: parent.height/25
            font.bold: true
        }
    }

    //PALABRA DE 9 NO ENCONTRADA
    //Mensaje - Encabezado 0.2 - Centro 0.6 (2*0.3) - Boton Continuar 0.2
    Rectangle {
        id: palabraDe9NoEncontrada

        anchors.centerIn: root
        visible: false
        width: root.width/1.5
        height: root.height/2
        radius: 4
        color: "red"
        border.width: 2
        border.color: "white"

        //Ponemos el texto con la ronda terminada
        Rectangle {
            id: fondoRonda
            anchors.bottom: parent.top
            anchors.horizontalCenter: parent.horizontalCenter
            height: parent.height*0.1
            width: parent.width*0.6
            color: "brown"
            radius: 4

            Text {
                id: textoRonda
                anchors.fill: parent
                anchors.centerIn: parent
                text: ""
                font.family: "Ubuntu"
                color: "white"
                font.pixelSize: height*0.8
                font.bold: true
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
            }
        }

        Image {
            id: cronoImagen
            source: "qrc:/images/images/crono.png"
            height: parent.height*0.2
            width: height
            anchors.top: palabraDe9NoEncontrada.top
            anchors.left: palabraDe9NoEncontrada.left

            Text {
                id: textoTiempoAgotado
                anchors.verticalCenter: parent.verticalCenter
                anchors.left: parent.right
                height: parent.height/2
                text: "¡TIEMPO AGOTADO!"
                color: "white"
                verticalAlignment: Text.AlignVCenter
                font.pixelSize: cronoImagen.height*0.28
                font.bold: true

            }
        }

        Text {
            id: palabraEncontradaJugador
            anchors.horizontalCenter: parent.horizontalCenter
            width: parent.width*0.9
            height: parent.height*0.25
            anchors.top: cronoImagen.bottom
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            wrapMode: Text.Wrap
            text: "Has encontrado la palabra:"
            color: "white"
            font.pixelSize: tamañoFuenteMediana
            font.bold: true

            Rectangle {
                id: rectPalabraJugador
                width: parent.width*0.6
                height: parent.height/3
                color: "black"
                anchors.bottom: parent.bottom
                anchors.horizontalCenter: parent.horizontalCenter

                Text {
                    anchors.centerIn: parent
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter
                    color: "white"
                    text: palabraMasLarga
                    font.pixelSize: cronoImagen.height*0.3
                    font.bold: true
                }
            }

            BotonImagen {
                id: lanzarRAEJugador

                width: parent.height*0.55
                height: width
                imagePath: "qrc:/images/images/diccionario.png"
                anchors.top: rectPalabraJugador.top
                anchors.left: rectPalabraJugador.right
                anchors.leftMargin: width/5
                onPulsado:  mostrarDiccionario(palabraMasLarga)

                Text {
                    anchors.top: parent.bottom
                    anchors.horizontalCenter: parent.horizontalCenter
                    width: parent.width
                    text: "Dic. RAE"
                    font.pixelSize: width/4
                    color: "white"
                }
            }
        }

        Text {
            id: palabraEncontradaCerebro
            anchors.horizontalCenter: parent.horizontalCenter
            width: parent.width*0.9
            height: parent.height*0.25
            anchors.top: palabraEncontradaJugador.bottom
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            wrapMode: Text.Wrap
            text: "Cerebro ha encontrado:"
            color: "white"
            font.pixelSize: tamañoFuenteMediana
            font.bold: true

            Rectangle {
                id: rectCerebro
                width: parent.width*0.6
                height: parent.height/3
                color: "black"
                anchors.bottom: parent.bottom
                anchors.horizontalCenter: parent.horizontalCenter

                Text {
                    id: palabraMaximaConseguidaCerebro
                    anchors.centerIn: parent
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter
                    color: "white"
                    text: ""
                    font.pixelSize: cronoImagen.height*0.3
                    font.bold: true
                }
            }

            BotonImagen {
                id: lanzarRAE

                width: parent.height*0.55
                height: width
                imagePath: "qrc:/images/images/diccionario.png"
                anchors.top: rectCerebro.top
                anchors.left: rectCerebro.right
                anchors.leftMargin: width/5
                onPulsado:  mostrarDiccionario(palabraMaximaConseguidaCerebro.text)

                Text {
                    anchors.top: parent.bottom
                    anchors.horizontalCenter: parent.horizontalCenter
                    width: parent.width
                    text: "Dic. RAE"
                    font.pixelSize: width/4
                    color: "white"
                }
            }
        }

        //FIJO EN TODOS LOS MENSAJES DE FIN DE PARTIDA (imagen y texto continuar partida) ----
        BotonImagen
        {
            id: btnContinuar2
            name: "btnContinuar"
            height: parent.height*0.15
            width: height

            anchors.bottom: txtContinuar2.top
            anchors.horizontalCenter: parent.horizontalCenter
            imagePath: "qrc:/images/images/siguiente.png"
            onPulsado: continuar(palabraMasLarga.length)
        }

        Text {

            id: txtContinuar2
            text: "Continuar"
            height: parent.height*0.05
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.bottom: parent.bottom
            anchors.bottomMargin: btnContinuar2.height/8
            color: "white"
            font.pixelSize: parent.height/25
            font.bold: true
        }
    }


// -- SLOTS ------------------------------------------------------------------------------

    onNumeroDeFichaChanged: {
        if (numeroDeFicha > 8)
            todasSacadas = true
        else
            todasSacadas = false
    }

    onTodasSacadasChanged: {
        if (todasSacadas)
        {
            //añadimos la decima letra invisible
            var data = {"index":numeroDeFicha, "valor":''}
            modeloLetras.append(data)
            //console.log ("10 letra = "+modeloLetras.get(9).valor)

            crono.activo = true
            crono.arrancarPararCrono()
            gridLetras.activo = true
            botonera.mezclarActivo = true
            console.log("===== INICIO PARTIDA =====")
            //Buscamos la palabra mas larga con estas letras
            palabraMaximaConseguidaCerebro.text = receiver.buscaPalabra()
            palabraMaximaConseguidaCerebroDe9.text = palabraMaximaConseguidaCerebro.text
            receiver.resetLetras()
            if (empezarEnPausa)
                pausarPartida()
        }
    }

    onPalabraChanged: {
        if (!deseleccionando)
        {
            //Comprobar palabra instantaneamente
            if (receiver.existePalabra(palabra))
            {
                if (palabra.length > palabraMasLarga.length)
                    palabraMasLarga = palabra
                gridPalabra.colorTexto = "green"
            }
            else
                gridPalabra.colorTexto = "red"
        }
    }

// -- FUNCIONES de ACTUALIZACION de los MODELOS ----------------------------------------

    Timer {
        id: timer
        interval: 300
        running: true
        repeat: todasSacadas ? false : true
        onTriggered: addItemModeloLetras()
    }

    function addItemModeloLetras()
    {
        if (!todasSacadas)
        {
            var letra = Letras.getLetraPartida()
            console.log ("LETRA " + numeroDeFicha + ": " + letra)
            var data = {"index":numeroDeFicha, "valor":letra}
            modeloLetras.append(data)
            letraTemp = letra
            receiver.addLetra(letraTemp)
            numeroDeFicha++
        }
    }

    function addItemModeloPalabra(indice)
    {
        var letra = modeloLetras.get(indice).valor
        //console.log("numero de letras palabra = "+numeroDeLetrasPalabra)
        if (numeroDeLetrasPalabra < 9)
        {
            palabra = palabra + letra

            var data = {"indexLetraPequena":numeroDeLetrasPalabra, "indexLetraGrande":indice, "valor":letra}
            modeloPalabra.append(data)
            if (insertarMov)
                Letras.pushMovimiento(indice)
            else
                insertarMov = true
            botonera.mostrarDeshacerYReiniciar()
        }
        actualizarIndices()
    }

    function delItemModeloPalabra(indicePalabra)
    {
        if (indicePalabra !== -1)
        {
            var indiceEnLetras = modeloPalabra.get(indicePalabra).indexLetraGrande
            //console.log("IndiceEnLetras = "+indiceEnLetras)
            modeloPalabra.remove(indicePalabra,1)
            if (insertarMov)
                Letras.pushMovimiento(indiceEnLetras)
            else
                insertarMov = true
        }
        else
            console.log("Error!! Intentamos eliminar una letra con indice palabra -1")

        actualizarIndices()
        //Si al eliminar la ultima letra eliminamos los movimientos y permitimos mezclar palabras
        if (palabra == "")
        {
            Letras.resetMovimientos()
            botonera.mostrarSoloMezclar()
        }
    }

    function  actualizarIndices()
    {
        var palabraAux = ""
        for (var i= 0; i< modeloPalabra.count; i++)
        {
            //en el modelo de la palabra
            modeloPalabra.get(i).indexLetraPequena = i
            //la palabra es igual a los elementos del modeloPalabra concatenados
            palabraAux = palabraAux + modeloPalabra.get(i).valor
            //en la Letra
            gridLetras.currentIndex = modeloPalabra.get(i).indexLetraGrande
            gridLetras.currentItem.indiceEnPalabra = i
        }
        palabra = palabraAux
    }

// ---------------- PAUSA y FIN de partida  ----------------------------
    function acabarPartida()
    {
        partidaTerminada = true
        gridLetras.visible = false
        gridPalabra.visible = false
        crono.activo = false
        botonera.ocultarTodo()
    }

    //pausa y da al play a la partida
    function pausarPartida()
    {
        //Si el crono no esta activo no se puede pausar la partida
        //El crono se activa al iniciarse y no se desactiva hasta llega a cero aunque se pare
        if (!crono.activo)
        {
            return
        }

        //si estaba pausada , la reactivamos
        if (partidaPausada)
        {
            partidaPausada = false
            gridLetras.visible = true
            botonPlay.visible = false
            botonera.visible = true
            popup.cambiarIcono(1,"qrc:/images/images/pause.png")
            popup.cambiarTexto(1,"Pausa")
            console.log("===== PARTIDA REANUDADA =====")
        }
        //la pausamos
        else
        {
            partidaPausada = true
            gridLetras.visible = false
            botonera.visible = false
            botonPlay.visible = true
            popup.cambiarIcono(1,"qrc:/images/images/play.png")
            popup.cambiarTexto(1,"Reanudar")
            console.log("===== PARTIDA PAUSADA ======")
        }
        empezarEnPausa = false
        crono.arrancarPararCrono()
    }

// ----- Modelos de datos ---------------------------------------------

    //Los elementos de todos los modelos son "index" que es el indice que ocupa dentro del modelo
    // y "valor" que es el valor de la letra que almacena

    //Lista con las letras con las que jugamos en la partida en la partida
    ListModel {
        id: modeloLetras
     }

    //Lista con la palabra que estamos formando
    ListModel {
        id: modeloPalabra
     }


 // -- POPUP / Lanzador -----------------------------------------

    Popup {

        id: popup
        anchors.fill: root
        modelo: ModeloPopup {}
        onElementoPulsado: {
            switch(index)
            {
                case 1:
                    Letras.reiniciar()
                    cambiarQml("MenuPrincipal.qml")
                    break
                case 2://pausa
                    pausarPartida()
                    break
                case 3: //ayuda
                    if (!todasSacadas)
                        empezarEnPausa = true
                    else if(!partidaPausada)
                        pausarPartida()
                    ayuda(3) //es una partida completa
                    break
                case 4://reiniciar partida completa
                    Letras.reiniciar()
                    botonPlay.visible = false
                    reiniciarPartidaCompleta()
                    break
                default:
                    break
            }
        }
    }

//Fin fichero
}
