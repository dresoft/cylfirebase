import QtQuick 2.3
import QtQuick.Controls 1.2


Item {

    id: root

    //variables del modelo de datos
    property string text: texto.text
    property int indiceLetraPequena //lugar que ocupa la letra en el modelo de letras pequeñas
    property int indiceLetraGrande //lugar que ocupa la letra en el modelo de letras grandes


    property string colorTexto
    property string colorFondo
    property int tamanoFuenteDefecto: height/1.2

    property bool enPruebas: false //Lo activamos para ver los superindices en pruebas

    property bool activo

    signal letraPulsada (int index)

    Rectangle {

        id: fondo
        width: parent.width
        height: parent.height
        anchors.centerIn: parent
        radius: 4
        color: "white"
    }

    Text {
        id: texto
        anchors.fill: fondo
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignHCenter
        color: "#000000"
        text: root.text
    }

    //Texto para probar, normalmente visible false
    Text {
        id: textoIndiceLetraGrande
        anchors.horizontalCenter: fondo.horizontalCenter
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignHCenter
        color: "black"
        text: indiceLetraGrande
        font.pixelSize: parent.height/5
        visible: enPruebas
    }

    MouseArea {

        id: mouseArea
        anchors.fill: root
        onClicked:
        {
            //Nothing to do
        }
    }

    onTextChanged: ajustarTexto()
    onColorFondoChanged: fondo.color = colorFondo
    onColorTextoChanged: texto.color = colorTexto

    Component.onCompleted: ajustarTexto()

// --- Funciones ------------------------------------------------------------------

    function ajustarTexto ()
    {
        if (texto.text == 'W')
        {
            texto.font.pixelSize = tamanoFuenteDefecto*0.75
            return
        }

        if (text.length > 3)
            texto.font.pixelSize = tamanoFuenteDefecto * (3/text.length)
        else
            texto.font.pixelSize = tamanoFuenteDefecto
    }
}
