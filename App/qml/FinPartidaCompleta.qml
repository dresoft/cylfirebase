import QtQuick 2.0

import "../javascript/PartidaCompleta.js" as Partida
import "../javascript/Records.js" as Records

Item {

    id: finPartida
    visible: false

    property int resultadoFinal: 0

    signal home
    signal reiniciarPartidaCompleta
    signal tecladoCerr
    signal tecladoAbie

    Rectangle {
        id: fondo
        anchors.fill: parent
        color: "#333333"
    }

    // ---- 35% parte SUPERIOR - Textos y puntuaciones -------------------------------------
    Rectangle {
        id: fondoGris
        height: parent.height*0.7
        width: parent.width*0.8
        anchors.centerIn: parent
        color: "lightgray"
        radius: 4
        border.width: 4
        border.color: "white"

        Text {
            id: txtFinPartida
            anchors.top: parent.top
            anchors.horizontalCenter: parent.horizontalCenter
            height: parent.height*0.15
            text: "Partida Terminada"
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            font.pixelSize: height*0.3
            font.family: "Ubuntu Mono"
            font.bold: true
        }

        Text {
            id: txtPuntuacionCifras1
            anchors.top: txtFinPartida.bottom
            anchors.left: parent.left
            height: parent.height*0.05
            width: parent.width*0.35
            text: Partida.puntuacionCifras
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignRight
            font.pixelSize: height*0.8
            font.family: "Sawasdee"
            color: "blue"
            font.bold: true
        }

        Text {
            id: txtPuntuacionCifras2
            anchors.top: txtFinPartida.bottom
            anchors.right: parent.right
            height: parent.height*0.05
            width: parent.width*0.65
            text: "PuntosCifras"
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignLeft
            font.pixelSize: height*0.7
            font.family: "Ubuntu Mono"
            font.bold: true

        }

        Text {
            id: txtPuntuacionLetras1
            anchors.top: txtPuntuacionCifras1.bottom
            anchors.left: parent.left
            height: parent.height*0.05
            width: parent.width*0.35
            text: Partida.puntuacionLetras
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignRight
            font.pixelSize: height*0.8
            font.family: "Sawasdee"
            color: "red"
            font.bold: true

        }

        Text {
            id: txtPuntuacionLetras2
            anchors.top: txtPuntuacionCifras1.bottom
            anchors.right: parent.right
            height: parent.height*0.05
            width: parent.width*0.65
            text: "PuntosLetras"
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignLeft
            font.pixelSize: height*0.7
            font.family: "Ubuntu Mono"
            font.bold: true

        }

        Rectangle {
            id: rectanguloBlanco
            anchors.top: txtPuntuacionLetras1.bottom
            anchors.horizontalCenter: parent.horizontalCenter
            height: parent.height*0.1
            width: parent.width*0.55
            radius: 4
            color: "tan"
        }

        Text {
            id: txtPuntuacionTotal1
            anchors.top: txtPuntuacionLetras1.bottom
            anchors.left: parent.left
            height: parent.height*0.1
            width: parent.width*0.35
            text: Partida.puntuacionTotal
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignRight
            font.pixelSize: height*0.7
            font.family: "Sawasdee"
            color: "purple"
            font.bold: true
        }

        Text {
            id: txtPuntuacionTotal2
            anchors.top: txtPuntuacionLetras1.bottom
            anchors.right: parent.right
            height: parent.height*0.1
            width: parent.width*0.65
            text: "PuntosTotales"
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignLeft
            font.pixelSize: height*0.4
            font.family: "Ubuntu Mono"
            font.bold: true
        }

        Image {
            id: iconoMedalla
            anchors.left: rectanguloBlanco.right
            anchors.verticalCenter: txtPuntuacionTotal2.verticalCenter
            anchors.leftMargin: width*0.4
            width: parent.width*0.10
            height: width
        }

    // ---- 50% parte MEDIA - Textos(10%) e Iniciales (40%) o diploma en su defecto -------------

        Text {
            id: resultadoGeneral
            height: parent.height*0.2
            width: parent.width*0.6
            anchors.top: txtPuntuacionTotal1.bottom
            anchors.horizontalCenter: parent.horizontalCenter
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            font.pixelSize: height*0.3
            font.bold: true
            font.family: "Ubuntu Mono"
            text: ""
        }

        Iniciales {
            id: iniciales
            visible: false
            anchors.top: resultadoGeneral.bottom
            anchors.horizontalCenter: parent.horizontalCenter
            height: parent.height*0.3
            width: parent.width*0.9
            onTecladoAb: tecladoAbie()
            onTecladoCe: tecladoCerr()
        }

        Image {
            id: imagenDiploma
            anchors.top: resultadoGeneral.bottom
            anchors.horizontalCenter: parent.horizontalCenter
            height: parent.height*0.30
            width: parent.width*0.55
            visible: false
            source: "qrc:/images/images/diploma.png"
        }

    // ---- 15% parte INFERIOR ------ Botones home y reiniciar

        Rectangle {

            id: fondoBotonHome
            height: parent.height*0.15
            width: parent.width*0.45
            anchors.left: parent.left
            anchors.bottom: parent.bottom
            color: "transparent"

            BotonImagen {
                id: botonHome
                anchors.right: parent.right
                anchors.verticalCenter: parent.verticalCenter
                height: parent.height*0.8
                width: height
                imagePath: "qrc:/images/images/home.png"
                onPulsado: home()
            }
        }

        Rectangle {

            id: fondoBotonReiniciar
            height: parent.height*0.15
            width: parent.width*0.45
            anchors.right: parent.right
            anchors.bottom: parent.bottom
            color: "transparent"

            BotonImagen {
                id: botonReiniciar
                height: parent.height*0.8
                width: height
                anchors.left: parent.left
                anchors.verticalCenter: parent.verticalCenter
                imagePath: "qrc:/images/images/reiniciarPartida.png"
                onPulsado: reiniciarPartidaCompleta()
            }
        }
    }

//-- FUNCIONES --------------------------------------------------------------------

    function getTextoResultadoPrincipal()
    {
        var salida = ""
        switch (Records.puestoLogrado)
        {
            case 1:
                salida = "¡¡¡ Glorioso Oro !!!"
                iniciales.visible = true
                iconoMedalla.source = "qrc:/images/images/iconoOro.png"
                break
            case 2:
                salida = "Mediocre Plata"
                iniciales.visible = true
                iconoMedalla.source = "qrc:/images/images/iconoPlata.png"
                break
            case 3:
                salida = "Bochornoso Bronce"
                iniciales.visible = true
                iconoMedalla.source = "qrc:/images/images/iconoBronce.png"
                break
            case 4:
                salida = "Diploma Olímpico"
                imagenDiploma.visible = true
                iconoMedalla.source = "qrc:/images/images/triste.png"
                break
        }

        return salida
    }

    function pintarResultados()
    {
        txtPuntuacionCifras1.text = Partida.puntuacionCifras
        txtPuntuacionLetras1.text = Partida.puntuacionLetras
        txtPuntuacionTotal1.text = Partida.puntuacionTotal
        resultadoGeneral.text = getTextoResultadoPrincipal()
    }

    // -- POPUP / Lanzador ---------------- Sobre la pantalla -------------------------

    Popup {

        id: popup
        anchors.fill: finPartida
        modelo: ModeloPopupFinPartida {}
        onElementoPulsado: {
            console.log("OPERACION PULSADA " + index)
            Partida.resetPartida()
            switch(index)
            {
                case 1:
                    cambiarQml("MenuPrincipal.qml")
                    break;
                case 2://reiniciar
                    reiniciarPartidaCompleta();
                    break;
                default:
                    break;
            }
        }
    }
}

