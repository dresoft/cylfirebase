import QtQuick 2.0

import "../javascript/Records.js" as Records

Item {

    id: records
    signal cambiarQml (string url)  //SEÑAL COMPARTIDA por todas las pantallas
    signal ayuda (int idPantalla)   //SEÑAL COMPARTIDA por todas las pantalla

    Rectangle {
        id: fondo
        anchors.fill: parent
        color: "#333333"
    }

    Text {
        id: cuadroTexto
        height: parent.height*0.15
        width: parent.width*0.8
        anchors.top: parent.top
        anchors.right: parent.right
        text: "Récords"
        color: "white"
        font.bold: true
        font.pixelSize: cuadroTexto.height*0.5
        font.family: "Ubuntu Mono"
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignHCenter
    }

    // ----------------- Oro ----------------------------------
    Rectangle {
        id: oro
        anchors.top: cuadroTexto.bottom
        anchors.left: parent.left
        width: parent.width
        height: parent.height*0.25

        Image {
            id: imgOro
            anchors.left: parent.left
            anchors.verticalCenter: parent.verticalCenter
            width: parent.width * 0.3
            height: width
            source: "qrc:/images/images/gold_medal.png"
        }

        Text {
            id: nombreOro
            anchors.left: imgOro.right
            anchors.top: parent.top
            height: parent.height
            width: parent.width*0.5
            font.pixelSize: height*0.15
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            text: Records.nombreOro
        }

        Text {
            id: puntuacionOro
            anchors.right: parent.right
            anchors.top: parent.top
            anchors.rightMargin: height*0.1
            height: parent.height
            width: parent.width*0.2
            font.pixelSize: height*0.2
            font.bold: true
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            text: Records.puntuacionOro
        }
    }


    // ----------------- Plata ----------------------------------
    Rectangle {
        id: plata
        anchors.top: oro.bottom
        anchors.left: parent.left
        width: parent.width
        height: parent.height*0.25

        Image {
            id: imgPlata
            anchors.left: parent.left
            anchors.verticalCenter: parent.verticalCenter
            width: parent.width * 0.3
            height: width
            source: "qrc:/images/images/silver_medal.png"
        }

        Text {
            id: nombrePlata
            anchors.left: imgPlata.right
            anchors.top: parent.top
            height: parent.height
            width: parent.width*0.5
            font.pixelSize: height*0.15
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            text: Records.nombrePlata
        }

        Text {
            id: puntuacionPlata
            anchors.right: parent.right
            anchors.top: parent.top
            anchors.rightMargin: height*0.1
            height: parent.height
            width: parent.width*0.2
            font.pixelSize: height*0.2
            font.bold: true
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            text: Records.puntuacionPlata
        }
    }


    // ----------------- Bronce ----------------------------------
    Rectangle {
        id: bronce
        anchors.top: plata.bottom
        anchors.left: parent.left
        width: parent.width
        height: parent.height*0.25

        Image {
            id: imgBronce
            anchors.left: parent.left
            anchors.verticalCenter: parent.verticalCenter
            width: parent.width * 0.3
            height: width
            source: "qrc:/images/images/bronze_medal.png"
        }

        Text {
            id: nombreBronce
            anchors.left: imgBronce.right
            anchors.top: parent.top
            height: parent.height
            width: parent.width*0.5
            font.pixelSize: height*0.15
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            text: Records.nombreBronce
        }

        Text {
            id: puntuacionBronce
            anchors.right: parent.right
            anchors.top: parent.top
            anchors.rightMargin: height*0.1
            height: parent.height
            width: parent.width*0.2
            font.pixelSize: height*0.2
            font.bold: true
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            text: Records.puntuacionBronce
        }
    }

    Text {
        id: maxPosible
        anchors.top: bronce.bottom
        anchors.right: parent.right
        anchors.rightMargin: height*0.1
        width: parent.width*0.5
        height: parent.height*0.1
        font.pixelSize: height*0.2
        color: "lightgray"
        text: "* Máx.puntuación posible: 86"
        font.family: "Ubuntu Mono"
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignHCenter

    }

    BotonImagen {
        id: borrarRecords
        anchors.top: bronce.bottom
        anchors.left: parent.left
        anchors.leftMargin: height*0.1
        height: parent.height*0.09
        width: height
        imagePath: "qrc:/images/images/papeleraVacia.png"
        onPulsado: {
            if (receiver.reiniciarRecords())
            {
                console.log("Records reiniciados")
                receiver.setRecords()
                actualizarPantalla()
                txtBorrarRecords.color = "red"
                txtBorrarRecords.text = "Récords borrados"
                activo = false
                enabled = false
                cambiarImagen("qrc:/images/images/papelera.png")
                Records.resetRecords()
            }
            else
            {
                console.log("Error al reiniciar records")
            }
        }
    }

    Text {
        id: txtBorrarRecords
        anchors.top: bronce.bottom
        anchors.left: borrarRecords.right
        height: parent.height*0.1
        width: height
        text: "Borrar récords"
        font.pixelSize: height*0.16
        color: "lightblue"
        verticalAlignment: Text.AlignVCenter
    }

    function actualizarPantalla()
    {
        nombreOro.text = receiver.nombreOro
        nombrePlata.text = receiver.nombrePlata
        nombreBronce.text = receiver.nombreBronce
        puntuacionOro.text = receiver.puntosOro
        puntuacionPlata.text = receiver.puntosPlata
        puntuacionBronce.text = receiver.puntosBronce
    }

    // -- POPUP / Lanzador ---------------- Sobre la pantalla -------------------------

       Popup {

           id: popup
           anchors.fill: records
           modelo: ModeloPopupRecords {}
           onElementoPulsado: {
               console.log("OPERACION PULSADA " + index)
               switch(index)
               {
                   case 1: cambiarQml("MenuPrincipal.qml")
                           break;
                   case 2://ayuda
                           ayuda(4);
                           break;
                   default:
                           break;
               }
           }
       }
}

