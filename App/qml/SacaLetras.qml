import QtQuick 2.3
import QtQuick.Controls 1.2


Rectangle {

    id: sacaletras
    width: 135
    height: 120
    color: "darkblue"
    border.color: "#000000"
    border.width: 2

    signal vocalPedida
    signal consonantePedida

    property int value: 1
    property bool activo: false;

    Rectangle {
            id: vocales
            width: 60
            height: 85

            color: "deeppink"
            anchors.left: sacaletras.left
            anchors.leftMargin: 5
            y: sacaletras.height/5
            border.color: "#FFFFFF"
            border.width: 2
            opacity: mouseAreaVoc.containsMouse ? 0.8 : 1.0

            Text {
                anchors.centerIn: vocales
                text: "V"
                font.pixelSize: 80
                color: "#FFFFFF"
            }

            MouseArea {
                id: mouseAreaVoc
                anchors.fill: vocales
                onClicked: tablero.vocalPedida();
            }
        }

        Rectangle {
            id: consonantes
            width: 60
            height: 85

            color: "deeppink"
            anchors.left: vocales.right
            anchors.leftMargin: 5
            y: sacaletras.height/5
            border.color: "#FFFFFF"
            border.width: 2
            opacity: mouseAreaCon.containsMouse ? 0.8 : 1.0

            MouseArea {
                id: mouseAreaCon
                anchors.fill: consonantes
                onClicked: tablero.consonantePedida()
            }

            Text {
                anchors.centerIn: consonantes
                text: "C"
                font.pixelSize: 80
                color: "#FFFFFF"
            }
        }

        onActivoChanged: {
            if (activo)
                timer.running = true;
            else
            {
                timer.running = false;
                consonantes.border.color = "#FFFFFF";
                vocales.border.color = "#FFFFFF";
                value = 1;
            }
        }

        //Highligthing de los borders para avisar de que está activo
        Timer {
             id: timer
             interval: 200; running: false; repeat: true
             onTriggered: {
                 switch (value)
                 {
                    case 1: vocales.border.color = "white";
                        consonantes.border.color = "white";
                        value++;
                        break;
                    case 2: vocales.border.color = "lightyellow";
                        consonantes.border.color = "lightyellow";
                        value++;
                        break;
                    case 3: vocales.border.color = "yellow";
                        consonantes.border.color = "yellow";
                        value++;
                        break;
                    case 4: vocales.border.color = "yellow";
                        consonantes.border.color = "yellow";
                        value++;
                        break;
                    case 5: vocales.border.color = "lightyellow";
                        consonantes.border.color = "lightyellow";
                        value = 1;
                        break;
                 }
            }
        }
}
//FIN SACALETRAS
