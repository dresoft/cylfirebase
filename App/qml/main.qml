import QtQuick 2.3
import QtQuick.Controls 1.2
import QtQuick.Window 2.2
import QtFirebase 1.0

import "../javascript/Records.js" as Records
import "../javascript/PartidaCompleta.js" as Partida
import "../javascript/Cifras.js" as Cifras
import "../javascript/Letras.js" as Letras


ApplicationWindow {


    visible: true
    title: qsTr("Cifras y Letras")

    id: main
    objectName: "main"
    property bool autenticado: false

//16:9 - Para pruebas LG - G3
//   property int anchoPantalla: 405
//   property int altoPantalla: 660
//Screen ajustable - Android
    property int anchoPantalla: Screen.desktopAvailableWidth
    property int altoPantalla: Screen.desktopAvailableHeight

    Item {

        id: fondo
        width: main.anchoPantalla
        height: main.altoPantalla
        focus: true

        Rectangle {
            anchors.fill: parent
            color: "#333333"
        }

        //Manejamos la tecla retroceder del movil
        Keys.onBackPressed: {
            console.log("Back button captured - Main.qml !")
            console.log("pageLoader.source: "+pageLoader.source)
            //Salimos
            if (navegador.visible)
            {
                navegador.visible = false
            }
            else if (pageLoader.source.toString() === "qrc:/qml/MenuPrincipal.qml")            {
                console.log("Salimos")
                Qt.quit()
            }
            else
            {
                if ((pageLoader.source.toString() === "qrc:/qml/PantallaPartidaCompleta.qml") ||
                    (pageLoader.source.toString() === "qrc:/qml/PantallaCifras.qml") ||
                    (pageLoader.source.toString() === "qrc:/qml/PantallaLetras.qml"))
                {
                    if (!avisoSalir.visible)
                    {
                        avisoSalir.visible = true
                    }
                    else
                    {
                        if (pageLoader.source.toString() === "qrc:/qml/PantallaPartidaCompleta.qml")
                        {
                            console.log("reseteamos partida")
                            Partida.resetPartida()
                        }
                        console.log("Volvemos al menu principal")
                        pageLoader.setSource("MenuPrincipal.qml")
                        temp.stop()
                        avisoSalir.visible = false
                    }
                }
                else
                {
                    console.log("Volvemos al menu principal")
                    pageLoader.setSource("MenuPrincipal.qml")
                }
            }
            event.accepter = true
        }


        /*
         * AdMob example
         */

        Timer {
                id: bannerRetryTimer
                interval: 30000
                property int reloads: 0
                onTriggered: {
                    if(reloads < 50) {
                        banner.load()
                        reloads++
                    }
                }
            }

        AdMob {
            //real
            appId: "ca-app-pub-2496446168567993~6309067466"
            //prueba
            //appId: Qt.platform.os == "android" ? "ca-app-pub-6606648560678905~6485875670" : "ca-app-pub-6606648560678905~1693919273"
            // NOTE All banners and interstitials will use this list
            // unless they have their own testDevices list specified
            testDevices: [
                "01987FA9D5F5CEC3542F54FB2DDC89F6",
                "d206f9511ffc1bc2c7b6d6e0d0e448cc"/*,
                "254232D5A701299C2EEAD823274C87D8"*/
            ]
        }

        // NOTE a size of 320x50 will give a Standard banner - other sizes will give a SmartBanner
        // NOTE width and height are values relative to the native screen size - NOT any parent QML components
        AdMobBanner {
            id: banner
            //real
            adUnitId: "ca-app-pub-2496446168567993/7785800666"
            //test
            //adUnitId: Qt.platform.os == "android" ? "ca-app-pub-3940256099942544/6300978111" : "ca-app-pub-6606648560678905/3170652476"

            x: fondo.width*0.08
            y: fondo.height*0.915

            visible: loaded

            width: 320
            height: 74

            onReadyChanged: if(ready) {
                                moveTo(fondo.width*0.06,fondo.height*0.915)
                                load()
                            }

            onError: {
                // TODO fix "undefined" arguments
                console.log("Banner failed with error code",code,"and message",message)

                // See AdMob.Error* enums
                if(code === AdMob.ErrorNetworkError)
                    console.log("No network available");

                bannerRetryTimer.restart()
            }

            request: AdMobRequest {}
        }

        Loader {
            id: pageLoader
            width: fondo.width
            height: fondo.height *0.88
            anchors.horizontalCenter: fondo.horizontalCenter
            anchors.top: fondo.root
            source: "MenuPrincipal.qml"
        }

        Connections {
            target: pageLoader.item
            ignoreUnknownSignals: true
            onCambiarQml:
            {
                console.log("== Cargamos " + url + " ==")
                receiver.resetCifras()
                receiver.resetLetras()
                Cifras.generaPartida()
                //Cifras.setPartida("999999999999999")
                pageLoader.setSource(url)
            }
            onAyuda: {
                ayuda.setPantalla(idPantalla)
                ayuda.visible = true
            }
            onTecladoAbierto: {
                banner.visible = false
            }
            onTecladoCerrado: {
                banner.visible = true
            }
            onAbrirDiccionario: {
                navegador.buscarPalabra(palabraABuscar)
            }
        }

        VistaWeb {
            id: navegador
            anchors.horizontalCenter: pageLoader.horizontalCenter
            anchors.bottom: pageLoader.bottom
            //dejamos de ver un 0.2 para no mostrar el menu de navegador que no consigo quitar con javascript
            height: pageLoader.height*1.2
            width: pageLoader.width
            visible: false
        }

        Ayuda {
            id: ayuda
            width: fondo.width
            height: fondo.height *0.88
            anchors.horizontalCenter: fondo.horizontalCenter
            anchors.top: fondo.root
            visible: false
        }

        /*
         * Analytics example
         */
        Analytics {
            id: analytics

            // Analytics collection enabled
            enabled: true

            // App needs to be open at least 1s before logging a valid session
            minimumSessionDuration: 1000
            // App session times out after 5s (5 seconds = 5000 milliseconds)
            sessionTimeout: 5000

            // Set the user ID:
            // NOTE the user id can't be more than 36 chars long
            //userId: "A_VERY_VERY_VERY_VERY_VERY_VERY_LONG_USER_ID_WILL_BE_TRUNCATED"
            userId: ""
            // or call setUserId()

            // Unset the user ID:
            // userId: "" or call "unsetUserId()"

            // Set user properties:
            // Max 25 properties allowed by Google
            // See https://firebase.google.com/docs/analytics/cpp/properties
            userProperties: []
            // or call setUserProperty()

            onReadyChanged: {
                // See: https://firebase.google.com/docs/analytics/cpp/events
                analytics.logEvent("qtfb_ready_event")
                analytics.logEvent("qtfb_ready_event","string_test","string")
                analytics.logEvent("qtfb_ready_event","int_test",getRandomInt(-100, 100))
                analytics.logEvent("qtfb_ready_event","double_test",getRandomArbitrary(-2.1, 2.7))

                analytics.logEvent("qtfb_ready_event_bundle",{
                    'key_one': 'value',
                    'key_two': 14,
                    'key_three': 2.3
                })
            }
        }


        Rectangle {
            id: avisoSalir
            anchors.centerIn: parent
            height: parent.height*0.10
            width: parent.width*0.45
            visible: false
            opacity: 0.8
            radius: 6
            color: "white"
            border.width: 2
            border.color: "black"
            onVisibleChanged: {
                if (!visible)
                {
                    temp.stop()
                    temp.running = false
                }
                else
                {
                    temp.start()
                }
            }

            Text {
                id: textoAvisoSalir
                anchors.fill: parent
                wrapMode: Text.WordWrap
                font.pixelSize: height*0.25
                color: "black"
                text: "Pulsa de nuevo retroceder para salir"
                font.family: "Ubuntu"
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
            }

            Timer {
                id: temp
                running: false
                interval: 1500
                onTriggered: avisoSalir.visible = false
            }
        }

        Component.onCompleted: {
            console.log("SCREEN WIDTH = " + Screen.desktopAvailableWidth);
            console.log("SCREEN HEIGHT = " + Screen.desktopAvailableHeight);
            console.log("BANNER WIDTH = ") + banner.width
            console.log("BANNER HEIGHT = ") + banner.height

            setRecords()
    //        textoAvisoSalir.visible = true
        }

        function setRecords()
        {
            Records.cargarPuntuacionOro(receiver.puntosOro,receiver.nombreOro)
            Records.cargarPuntuacionPlata(receiver.puntosPlata,receiver.nombrePlata)
            Records.cargarPuntuacionBronce(receiver.puntosBronce,receiver.nombreBronce)
        }
    }

    onAutenticadoChanged: {

                          }

}
