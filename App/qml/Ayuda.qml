import QtQuick 2.3
import QtQuick.Controls 1.4

import "../javascript/Textos.js" as Textos

Rectangle {

    id: screenAyuda
    property int pantalla: 0

    //anchors.fill: parent

    Rectangle {
        id: fondo
        anchors.fill: parent
        color: "gray"
    }

    Rectangle {
        id: fondoTexto
        anchors.centerIn: parent
        width: parent.width*0.8
        height: parent.height*0.8
        color: "white"
        border.width: 3
        border.color: "black"
        radius: 4


        Image {
            id: iconoAyuda
            anchors.top: parent.top
            anchors.left: parent.left
            width: parent.width*0.15
            height: width
            anchors.topMargin: width*0.15
            anchors.leftMargin: width*0.15
            source: "qrc:/images/images/ayuda.png"
        }

        Text {
            id: tituloAyuda
            height: iconoAyuda.height
            anchors.verticalCenter: iconoAyuda.verticalCenter
            anchors.horizontalCenter: parent.horizontalCenter
            width: parent.width
            text: "Ayuda"
            font.pixelSize: height*0.6
            font.family: "Ubuntu mono"
            font.bold: true
            font.underline: true
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
        }

        Text {
            id: tituloPantalla
            height: parent.height*0.1
            width: parent.width*0.9 //dejamos 0.1 de margen
            anchors.top: iconoAyuda.bottom
            anchors.horizontalCenter: parent.horizontalCenter
            text: "Ronda rapida cifras"
            font.bold: true
            font.family: "Ubuntu Mono"
            font.pixelSize: height*0.4
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignLeft
        }

        BotonImagen {
            id: cerrar
            imagePath: "qrc:/images/images/cerrar.png"
            anchors.bottom: fondoTexto.bottom
            anchors.bottomMargin: width * 0.1
            anchors.horizontalCenter: parent.horizontalCenter
            width: fondoTexto.width * 0.2
            height: width
            onPulsado: screenAyuda.visible = false
        }

        Flickable {
            id: flickArea
            anchors.top: tituloPantalla.bottom
            anchors.bottom: cerrar.top
//            anchors.bottomMargin: height*0.05
            anchors.horizontalCenter: parent.horizontalCenter
            width: parent.width*0.9
            contentWidth: width; contentHeight: texto.height
            flickableDirection: Flickable.VerticalFlick
            clip: true

            Text {
                id: texto
                width: parent.width*0.95 //dejamos 0.05 de margen
                anchors.horizontalCenter: parent.horizontalCenter
                wrapMode: Text.WordWrap
                font.pixelSize: tituloPantalla.font.pixelSize*0.5
                horizontalAlignment: Text.AlignLeft
                focus:true
            }
        }
    }

    Component.onCompleted: {
        console.log("Ayuda completada")
        actualizarTextos()
    }

    //-- Funciones --------------------------------------------------------------------

    function setPantalla(index)
    {
        pantalla = index
        //Actualizamos los textos cada vez que cambiamos de pantalla
        actualizarTextos()
    }

    function actualizarTextos()
    {
        tituloPantalla.text = Textos.getTituloAyuda(pantalla)
        texto.text = ""
        texto.text = Textos.getTextoAyuda(pantalla)
    }
}
